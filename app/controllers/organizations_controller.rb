class OrganizationsController < ApplicationController
  before_action :set_organization, only: [:show, :edit, :update, :destroy]
  before_action :set_mission, only: [:search]

  PATH_VALIDATOR = /\/organizations.*(term|page)/

  # GET /organizations
  # GET /organizations.json
  def index
    search_term = params.dig(:fulltext_search, :term)
    organizations = Organization
      .includes(:region, :areas, :structures, :market_targets, :business_markets)

    organizations = organizations.search_for(search_term) if search_term

    @organizations = organizations
      .order(:name)
      .paginate(page: params[:page], per_page: 20)
  end

  # # GET /organizations/1
  # # GET /organizations/1.json
  # def show
  # end

  # GET /organizations/new
  def new
    session[:redirect_to] ||= request.referer if request.referer.match(/\/organizations.*(term|page)/)
    @organization = Organization.new
  end

  # GET /organizations/1/edit
  def edit
    session[:redirect_to] ||= request.referer if request.referer.match(/\/organizations.*(term|page)/)
  end

  # POST /organizations
  # POST /organizations.json
  def create
    @organization = Organization.new(organization_params)

    respond_to do |format|
      if @organization.save
        redirection_url = get_redirection_url

        format.html { redirect_to redirection_url, notice: 'Organization was successfully created.' }
        format.json { render :show, status: :created, location: @organization }
      else
        format.html { render :new }
        format.json { render json: @organization.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /organizations/1
  # PATCH/PUT /organizations/1.json
  def update
    respond_to do |format|
      if @organization.update(organization_params)
        redirection_url = get_redirection_url
        format.html { redirect_to redirection_url, notice: 'Organization was successfully updated.' }
        format.json { render :show, status: :ok, location: @organization }
      else
        format.html { render :edit }
        format.json { render json: @organization.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /organizations/1
  # DELETE /organizations/1.json
  def destroy
    # puts ">"*50 + " " + request.referer.inspect
    @organization.destroy
    respond_to do |format|
      redirection_url = get_redirection_url
      format.html { redirect_to redirection_url, notice: 'Organization was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def search
    @organization = Organization
      .includes(:areas, :market_targets, :structures, :business_markets)
      .where(id: @mission.organization_id)
      .take
    function_id = @mission.function_id

    @organizations = Organization
      .joins(:areas)
      .includes(:areas)
      .joins(:functions)
      .includes(:functions)
      .joins(:market_targets)
      .includes(:market_targets)
      .joins(:structures)
      .includes(:structures)
      .joins(:business_markets)
      .includes(:business_markets)
      .joins(:region)
      .includes(:region)
      .where(areas: { id: @organization.area_ids } )
      .where(functions: { id: @mission.function_id } )
      .where(market_targets: { id: @organization.market_target_ids } )
      .where(structures: { id: @organization.structure_ids } )
      .where(business_markets: { id: @organization.business_market_ids } )
      .where(regions: { id: @organization.region_id })
      .where.not(id: @organization.id)
      .distinct
      .paginate(page: params[:page], per_page: 20)
  end

  private

    def get_redirection_url
      if session[:redirect_to].nil? && request.referer.match?(PATH_VALIDATOR)
        request.referer
      elsif session[:redirect_to].to_s.match?(PATH_VALIDATOR)
        session.delete(:redirect_to)
      else
        session.delete(:redirect_to)
        organizations_url
      end
    end

    def set_mission
      @mission = Mission.find(params[:mission_id])
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_organization
      @organization = Organization.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def organization_params
      params
        .require(:organization)
        .permit!
    end
end
