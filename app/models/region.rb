class Region < ApplicationRecord

  # Association(s):
  has_many :organizations

  # Validation(s):
  validates :code, :name, presence: true
  validates :code, :name, uniqueness: true

end
