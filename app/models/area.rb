class Area < ApplicationRecord
  include Labelization

  acts_as_nested_set

  # Association(s):
  has_many :areas, foreign_key: 'parent_id'
  has_and_belongs_to_many :organizations

end
