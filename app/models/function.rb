class Function < ApplicationRecord
  serialize :other_names, Array

  # Association(s):
  has_many :functions, foreign_key: 'parent_id'
  belongs_to :parent, foreign_key: 'parent_id', class_name: 'Function', optional: true

  # Validation(s):
  validates :name, presence: true
  validates :name, uniqueness: { scope: :parent_id, case_sensitive: false }

  # Scope(s):
  scope :roots, -> { where(parent_id: nil) }

  def to_label
    ([name] + other_names).join(' / ')
  end

end
