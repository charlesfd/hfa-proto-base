require 'active_support/concern'

module Labelization
  extend ActiveSupport::Concern

  included do
    scope :without_root, -> { where.not(depth: 0) }
  end

  def to_label
    self_and_ancestors
      .without_root
      .order('depth DESC')
      .pluck(:name)
      .join(' > ')
  end

end