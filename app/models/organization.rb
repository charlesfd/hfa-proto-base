class Organization < ApplicationRecord
  include PgSearch

  # Association(s):
  belongs_to :region, optional: true
  has_and_belongs_to_many :areas
  has_and_belongs_to_many :business_markets
  has_and_belongs_to_many :structures, join_table: 'structures_organizations'
  has_and_belongs_to_many :market_targets
  has_and_belongs_to_many :functions

  # Validation(s):
  validates :name, presence: true
  validates :name, uniqueness: true

  # Search:
  pg_search_scope :search_for, against: %i(name), using: {
    tsearch: { prefix: true, any_word: true }
  }

end
