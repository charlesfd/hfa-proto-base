class Structure < ApplicationRecord
  include Labelization

  acts_as_nested_set

  # Association(s):
  has_and_belongs_to_many :organizations, join_table: 'structures_organizations'

end
