class Mission < ApplicationRecord

  # Association(s):
  belongs_to :organization
  belongs_to :function
  has_and_belongs_to_many :areas
  has_and_belongs_to_many :structures
  has_and_belongs_to_many :business_markets
  has_and_belongs_to_many :market_targets
  belongs_to :region

  # Validation(s):
  validates :organization_id, presence: true
  validates :function_id, presence: true
  validates :title, presence: true

end
