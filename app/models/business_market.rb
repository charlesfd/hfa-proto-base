class BusinessMarket < ApplicationRecord
  include Labelization

  acts_as_nested_set

  # Association(s):
  has_and_belongs_to_many :organizations

end
