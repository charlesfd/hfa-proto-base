module OrganizationsHelper

  def display_taxonomies(collection)
    collection.map do |element|
      "<span class='badge badge-pill badge-info'>#{element.to_label}</span>"
    end.join('&nbsp;').html_safe
  end

  def display_region_name(region)
    return '' unless region
    region.name
  end

  def search_criteria(organization_name)
    case organization_name
    when "Tiller"
      {
        areas: Area.where(id: [56]),
        business_markets: BusinessMarket.where(id: [30]),
        structures: Structure.where(id: [5, 6, 10]),
        market_targets: MarketTarget.where(id: [2, 3]),
      }
    when "Synertrade"
      {
        areas: Area.where(id: [106, 112, 117, 120, 127]),
        business_markets: BusinessMarket.where(id: [43]),
        structures: Structure.where(id: [9, 10]),
        market_targets: MarketTarget.where(id: [2]),
      }
    when "Renovation Man"
      {
        areas: Area.where(id: [61, 63, 67, 70, 69, 72]),
        business_markets: BusinessMarket.where(id: [58]),
        structures: Structure.where(id: [4, 5]),
        market_targets: MarketTarget.where(id: [2, 3]),
      }
    else
      Hash.new([])
    end
  end
end
