module MissionsHelper

  def organizations_for_select
    @organizations_for_select ||= Organization.pluck(:name, :id)
  end

end
