module ApplicationHelper

  ####################### Public methods for Navbar ######################

  def navlink_active?(expected_controller_name)
    controller_name == expected_controller_name ? 'active' : nil
  end

  ########################################################################


  ############# Public methods for orms Criterias/Taxonomies #############

  def grouped_functions_for_select
    @grouped_functions_for_select ||= Function.roots.includes(:functions)
  end

  def nested_areas_for_select
    taxonomies_for_select(klass: Area)
  end

  def nested_business_markets_for_select
    taxonomies_for_select(klass: BusinessMarket)
  end

  def nested_structures_for_select
    taxonomies_for_select(klass: Structure)
  end

  def nested_market_targets_for_select
    taxonomies_for_select(klass: MarketTarget)
  end

  ########################################################################

private

  def taxonomies_for_select(klass:)
    return [] unless klass.any?

    mem = {}
    klass.root.descendants.map do |object|
      level = object.depth
      mem[level] = object
      name = mem.values.take(level).map(&:name).join(' > ')

      [name, object.id]
    end
  end

end
