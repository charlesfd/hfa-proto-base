Rails.application.routes.draw do
  resources :missions, except: [:show] do
    get 'organizations/search', to: 'organizations#search'
  end
  resources :organizations, except: [:show]

  root 'missions#index'
end
