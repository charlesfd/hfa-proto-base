HFAPrototypeBase::Application.configure do
  unless ENV['BASIC_AUTH'].blank?
    config.middleware.use ::Rack::Auth::Basic do |username, password|
      ENV['BASIC_AUTH'].split(';').any? do |pair|
        [username, password] == pair.split(':')
      end
    end
  end
end
