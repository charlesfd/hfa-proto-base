# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_05_18_091558) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "areas", force: :cascade do |t|
    t.string "name", null: false
    t.integer "parent_id"
    t.integer "lft", null: false
    t.integer "rgt", null: false
    t.integer "depth", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["lft"], name: "index_areas_on_lft"
    t.index ["parent_id"], name: "index_areas_on_parent_id"
    t.index ["rgt"], name: "index_areas_on_rgt"
  end

  create_table "areas_missions", id: false, force: :cascade do |t|
    t.bigint "mission_id", null: false
    t.bigint "area_id", null: false
    t.index ["area_id", "mission_id"], name: "index_area_id_mission_id"
    t.index ["mission_id", "area_id"], name: "index_mission_id_area_id"
  end

  create_table "areas_organizations", id: false, force: :cascade do |t|
    t.bigint "organization_id", null: false
    t.bigint "area_id", null: false
    t.index ["area_id", "organization_id"], name: "index_areas_organizations_on_area_id_and_organization_id", unique: true
    t.index ["organization_id", "area_id"], name: "index_areas_organizations_on_organization_id_and_area_id", unique: true
  end

  create_table "business_markets", force: :cascade do |t|
    t.string "name", null: false
    t.integer "parent_id"
    t.integer "lft", null: false
    t.integer "rgt", null: false
    t.integer "depth", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["lft"], name: "index_business_markets_on_lft"
    t.index ["parent_id"], name: "index_business_markets_on_parent_id"
    t.index ["rgt"], name: "index_business_markets_on_rgt"
  end

  create_table "business_markets_missions", id: false, force: :cascade do |t|
    t.bigint "mission_id", null: false
    t.bigint "business_market_id", null: false
    t.index ["business_market_id", "mission_id"], name: "index_business_market_id_mission_id"
    t.index ["mission_id", "business_market_id"], name: "index_mission_id_business_market_id"
  end

  create_table "business_markets_organizations", id: false, force: :cascade do |t|
    t.bigint "organization_id", null: false
    t.bigint "business_market_id", null: false
    t.index ["business_market_id", "organization_id"], name: "index_business_market_id_organization_id", unique: true
    t.index ["organization_id", "business_market_id"], name: "index_organization_id_business_market_id", unique: true
  end

  create_table "functions", force: :cascade do |t|
    t.string "name", null: false
    t.text "other_names"
    t.integer "parent_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "index_functions_on_name"
    t.index ["parent_id"], name: "index_functions_on_parent_id"
  end

  create_table "functions_organizations", id: false, force: :cascade do |t|
    t.bigint "organization_id", null: false
    t.bigint "function_id", null: false
    t.index ["function_id", "organization_id"], name: "index_function_id_organization_id"
    t.index ["organization_id", "function_id"], name: "index_organization_id_function_id"
  end

  create_table "market_targets", force: :cascade do |t|
    t.string "name", null: false
    t.integer "parent_id"
    t.integer "lft", null: false
    t.integer "rgt", null: false
    t.integer "depth", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["lft"], name: "index_market_targets_on_lft"
    t.index ["parent_id"], name: "index_market_targets_on_parent_id"
    t.index ["rgt"], name: "index_market_targets_on_rgt"
  end

  create_table "market_targets_missions", id: false, force: :cascade do |t|
    t.bigint "mission_id", null: false
    t.bigint "market_target_id", null: false
    t.index ["market_target_id", "mission_id"], name: "index_market_target_id_mission_id"
    t.index ["mission_id", "market_target_id"], name: "index_mission_id_market_target_id"
  end

  create_table "market_targets_organizations", id: false, force: :cascade do |t|
    t.bigint "organization_id", null: false
    t.bigint "market_target_id", null: false
    t.index ["market_target_id", "organization_id"], name: "index_market_target_id_organization_id", unique: true
    t.index ["organization_id", "market_target_id"], name: "index_organization_id_market_target_id", unique: true
  end

  create_table "missions", force: :cascade do |t|
    t.string "title", null: false
    t.bigint "organization_id", null: false
    t.bigint "function_id", null: false
    t.text "note"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "region_id"
    t.index ["function_id"], name: "index_missions_on_function_id"
    t.index ["organization_id"], name: "index_missions_on_organization_id"
    t.index ["region_id"], name: "index_missions_on_region_id"
  end

  create_table "missions_structures", id: false, force: :cascade do |t|
    t.bigint "mission_id", null: false
    t.bigint "structure_id", null: false
    t.index ["mission_id", "structure_id"], name: "index_mission_id_structure_id"
    t.index ["structure_id", "mission_id"], name: "index_structure_id_mission_id"
  end

  create_table "organizations", force: :cascade do |t|
    t.string "name", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "region_id"
    t.index ["name"], name: "index_organizations_on_name"
    t.index ["region_id"], name: "index_organizations_on_region_id"
  end

  create_table "regions", force: :cascade do |t|
    t.string "code", null: false
    t.string "name", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "index_regions_on_name"
  end

  create_table "structures", force: :cascade do |t|
    t.string "name", null: false
    t.integer "parent_id"
    t.integer "lft", null: false
    t.integer "rgt", null: false
    t.integer "depth", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["lft"], name: "index_structures_on_lft"
    t.index ["parent_id"], name: "index_structures_on_parent_id"
    t.index ["rgt"], name: "index_structures_on_rgt"
  end

  create_table "structures_organizations", id: false, force: :cascade do |t|
    t.bigint "organization_id", null: false
    t.bigint "structure_id", null: false
    t.index ["organization_id", "structure_id"], name: "index_organization_id_structure_id", unique: true
    t.index ["structure_id", "organization_id"], name: "index_structure_id_organization_id", unique: true
  end

  add_foreign_key "organizations", "regions"
end
