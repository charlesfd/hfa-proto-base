class AssociateTaxonomiesToOrgs < ActiveRecord::Migration[5.2]
  def change
    create_join_table :organizations, :areas do |t|
      t.index [:organization_id, :area_id], unique: true
      t.index [:area_id, :organization_id], unique: true
    end

    create_join_table :organizations, :business_markets do |t|
      t.index [:organization_id, :business_market_id], unique: true, name: 'index_organization_id_business_market_id'
      t.index [:business_market_id, :organization_id], unique: true, name: 'index_business_market_id_organization_id'
    end

    create_join_table :organizations, :structures, table_name: "structures_organizations" do |t|
      t.index [:organization_id, :structure_id], unique: true, name: 'index_organization_id_structure_id'
      t.index [:structure_id, :organization_id], unique: true, name: 'index_structure_id_organization_id'
    end

    create_join_table :organizations, :market_targets do |t|
      t.index [:organization_id, :market_target_id], unique: true, name: 'index_organization_id_market_target_id'
      t.index [:market_target_id, :organization_id], unique: true, name: 'index_market_target_id_organization_id'
    end
  end
end
