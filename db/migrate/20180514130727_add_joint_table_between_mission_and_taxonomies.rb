class AddJointTableBetweenMissionAndTaxonomies < ActiveRecord::Migration[5.2]
  def change
    create_join_table :missions, :areas do |t|
      t.index [:mission_id, :area_id], name: 'index_mission_id_area_id'
      t.index [:area_id, :mission_id], name: 'index_area_id_mission_id'
    end

    create_join_table :missions, :structures do |t|
      t.index [:mission_id, :structure_id], name: 'index_mission_id_structure_id'
      t.index [:structure_id, :mission_id], name: 'index_structure_id_mission_id'
    end

    create_join_table :missions, :business_markets do |t|
      t.index [:mission_id, :business_market_id], name: 'index_mission_id_business_market_id'
      t.index [:business_market_id, :mission_id], name: 'index_business_market_id_mission_id'
    end

    create_join_table :missions, :market_targets do |t|
      t.index [:mission_id, :market_target_id], name: 'index_mission_id_market_target_id'
      t.index [:market_target_id, :mission_id], name: 'index_market_target_id_mission_id'
    end

    add_column :missions, :region_id, :integer
    add_index :missions, :region_id
  end
end
