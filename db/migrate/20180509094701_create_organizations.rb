class CreateOrganizations < ActiveRecord::Migration[5.2]
  def change
    create_table :organizations do |t|
      t.string :name, null: false
      t.text :categories
      t.integer :nb_employees_min
      t.integer :nb_employees_max

      t.timestamps
      t.index :name
      t.index :nb_employees_min
      t.index :nb_employees_max
    end
  end
end
