class CreateBusinessMarkets < ActiveRecord::Migration[5.2]
  def change
    create_table :business_markets do |t|
      t.string :name, null: false
      t.integer :parent_id, index: true
      t.integer :lft, null: false, index: true
      t.integer :rgt, null: false, index: true

      t.integer :depth, null: false, default: 0

      t.timestamps
    end
  end
end
