class CreateFunctions < ActiveRecord::Migration[5.2]
  def change
    create_table :functions do |t|
      t.string :name, null: false
      t.text :other_names
      t.integer :parent_id, foreign_key: true

      t.timestamps
      t.index :name
      t.index :parent_id
    end
  end
end
