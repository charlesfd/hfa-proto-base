class AddTableForLocalizationOfOrgs < ActiveRecord::Migration[5.2]
  def change
    create_table :regions do |t|
      t.string :code, null: false
      t.string :name, null: false

      t.timestamps
      t.index :name
    end
  end
end
