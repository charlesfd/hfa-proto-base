class AssociationBetweenOrganizationsAndFunctions < ActiveRecord::Migration[5.2]
  def change
    create_join_table :organizations, :functions do |t|
      t.index [:organization_id, :function_id], name: 'index_organization_id_function_id'
      t.index [:function_id, :organization_id], name: 'index_function_id_organization_id'
    end
  end
end
