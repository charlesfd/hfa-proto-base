class CreateMissions < ActiveRecord::Migration[5.2]
  def change
    create_table :missions do |t|
      t.string :title, null: false
      t.references :organization, null: false
      t.references :function, null: false
      t.text :note

      t.timestamps
    end
  end
end
