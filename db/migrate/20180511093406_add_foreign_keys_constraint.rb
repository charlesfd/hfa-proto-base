class AddForeignKeysConstraint < ActiveRecord::Migration[5.2]
  def change
    add_reference :organizations, :region, foreign_key: true
  end
end
