class RemoveUselessOrganizationFields < ActiveRecord::Migration[5.2]
  def change
    remove_column :organizations, :categories
    remove_index :organizations, :nb_employees_min
    remove_column :organizations, :nb_employees_min
    remove_index :organizations, :nb_employees_max
    remove_column :organizations, :nb_employees_max
  end
end
