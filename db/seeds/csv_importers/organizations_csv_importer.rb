module Seeds
  module CsvImporters
    class OrganizationsCsvImporter < Base

      class << self

        def import(csv_file_path:)
          puts "[OrganizationsCsvImporter] Starting import..."

          process_infos = {}
          process_infos[:count] = 0

          csv_processor(csv_file_path, process_infos) do |row, _, _|
            # manage row
            organization_name = row[0].strip.gsub(/:?°?\s*,?\s*$/, '').titleize

            Organization.find_or_create_by!(name: organization_name) do |org|
              process_infos[:count] += 1 unless org.id
            end
          end

          info_message = "[OrganizationsCsvImporter] Import finished " + ">"*50
          puts info_message
          puts "Data treated #{process_infos[:imported_rows]}/#{process_infos[:rows]}"
          puts "Data saved #{process_infos[:count]}"
          puts "*"* info_message.size
        end

      end

    end
  end
end
