module Seeds
  module CsvImporters
    class TaxonomiesCsvImporter < Base

      class << self

        def import(klass:, csv_file_path:)
          puts "[TaxonomiesCsvImporter - #{klass.to_s}] Starting import..."

          process_infos = Hash.new(0)
          root = klass.new(name: 'root')

          csv_processor(csv_file_path, process_infos) do |row, zone_buffer, end_of_file|
            zone = klass.new(name: row[1])

            case row[0].to_s.downcase
            when 'zone 1'
              root.children << zone
              zone_buffer[:z1] = zone
            when 'zone 2'
              zone_buffer[:z1].children << zone
              zone_buffer[:z2] = zone
            when 'zone 3'
              zone_buffer[:z2].children << zone
              zone_buffer[:z3] = zone
            else
              zone_buffer[:z3].children << zone
            end
          end

          root.save!

          info_message = "[TaxonomiesCsvImporter - #{klass.to_s}] Import finished " + ">"*50
          puts info_message
          puts "Data treated #{process_infos[:imported_rows]}/#{process_infos[:rows]}"
          puts "Data saved #{klass.count} (including the root)"
          puts "*"* info_message.size
        end

      end

    end
  end
end
