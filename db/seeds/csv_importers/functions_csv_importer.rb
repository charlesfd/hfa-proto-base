module Seeds
  module CsvImporters
    class FunctionsCsvImporter < Base

      class << self

        def import(csv_file_path:)
          puts "[FunctionsCsvImporter] Starting import..."

          process_infos = {}
          process_infos[:to_process] = {}

          csv_processor(csv_file_path, process_infos) do |row, _, _|
            department = row[0]
            function = row[1]
            others = [row[2], row[3], row[4]].uniq.compact

            process_infos[:to_process][{ name: department}] ||= []
            process_infos[:to_process][{ name: department}] << { name: function, other_names: others }
          end

          process_infos[:to_process].each do |(department_infos, functions_infos)|
            department = Function.new(department_infos)
            department.functions.build(functions_infos)
            department.save!
          end

          info_message = "[FunctionsCsvImporter] Import finished " + ">"*50
          puts info_message
          puts "Data treated #{process_infos[:imported_rows]}/#{process_infos[:rows]}"
          puts "Data saved #{Function.count}"
          puts "*"* info_message.size
        end

      end

    end
  end
end
