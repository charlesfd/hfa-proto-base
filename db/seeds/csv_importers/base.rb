require 'csv'

module Seeds
  module CsvImporters
    class Base

      class << self

        def csv_processor(csv_file_path, process_infos)
          process_infos[:rows] = 0
          process_infos[:imported_rows] = 0

          File.open(csv_file_path) do |file|
            zone_buffer = {}

            file.each_line do |line|
              process_infos[:rows] += 1
              begin
                row = CSV.parse_line(line.scrub(""), col_sep: ';')
                yield(row, zone_buffer, file.eof?)

                process_infos[:imported_rows] += 1
              rescue StandardError => se
                puts se.message
                puts "Concerned row: #{line.inspect} - line: #{file.lineno}"
                # puts se.backtrace
                next
              end
            end
          end
        end

      end

    end
  end
end