require_relative 'csv_importers/base.rb'
require_relative 'csv_importers/functions_csv_importer.rb'

module Seeds
  class Functions
    include CsvImporters

    def self.import(file_path)
      FunctionsCsvImporter
        .import(csv_file_path: file_path)
    end

  end
end