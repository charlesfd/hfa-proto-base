module Seeds
  class Regions

    attr_reader :regions_list

    def initialize(regions_list:)
      @regions_list = regions_list
    end

    def import
      puts "[Regions import] Start importation..."
      Region.create!(regions_list)
      puts "[Regions import] data imported."
    end

  end
end