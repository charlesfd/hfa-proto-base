module Seeds
  class Organizations

    attr_reader :customers_and_relateds

    def initialize(customers_and_relateds:)
      @customers_and_relateds = customers_and_relateds
    end

    def import(region_code:)
      puts "[Customers & Related import] Start importation..."
      region = Region.find_by(code: region_code)
      orgs_infos = generate_orgs_infos(region)

      # Organization.create!(orgs_infos)
      orgs_infos.each do |org_infos|
        Organization.find_or_create_by!(org_infos)
      end

      puts "[Customers & Related import] data imported."
    end

    def generate_orgs_infos(region)
      customers_and_relateds.flat_map do |customer_and_relateds|
        list = customer_and_relateds.values.flatten
        list.each_with_object([]) do |org_name, acc|
          acc << { name: org_name, region: region }
        end
      end
    end

  end
end