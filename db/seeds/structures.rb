require_relative 'csv_importers/base.rb'
require_relative 'csv_importers/taxonomies_csv_importer.rb'

module Seeds
  class Structures
    include CsvImporters

    def self.import(file_path)
      TaxonomiesCsvImporter
        .import(klass: Structure, csv_file_path: file_path)
    end

  end
end