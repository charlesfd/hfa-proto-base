# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

REGIONS_LIST = [
  { code: "FR-ARA", name: "Auvergne-Rhône-Alpes" },
  { code: "FR-BFC", name: "Bourgogne-Franche-Comté" },
  { code: "FR-BRE", name: "Bretagne" },
  { code: "FR-CVL", name: "Centre-Val de Loire" },
  { code: "FR-COR", name: "Corse" },
  { code: "FR-GES", name: "Grand Est" },
  { code: "FR-HDF", name: "Hauts-de-France" },
  { code: "FR-IDF", name: "Île-de-France" },
  { code: "FR-NOR", name: "Normandie" },
  { code: "FR-NAQ", name: "Nouvelle-Aquitaine" },
  { code: "FR-OCC", name: "Occitanie" },
  { code: "FR-PDL", name: "Pays de la Loire" },
  { code: "FR-PAC", name: "Provence-Alpes-Côte d'Azur" },
  { code: "FR-GP", name: "Guadeloupe" },
  { code: "FR-GF", name: "Guyane" },
  { code: "FR-MQ", name: "Martinique" },
  { code: "FR-RE", name: "La Réunion" },
  { code: "FR-YT", name: "Mayotte" },
].freeze

# Seed Areas:
require_relative 'seeds/areas.rb'
areas_file_path = File.join(__dir__, 'seeds', 'data', 'areas.csv')
Seeds::Areas.import(areas_file_path) if File.exists?(areas_file_path)

# Seed BusinessMarkets:
require_relative 'seeds/business_markets.rb'
business_markets_file_path = File.join(__dir__, 'seeds', 'data', 'business_markets.csv')
Seeds::BusinessMarkets.import(business_markets_file_path) if File.exists?(business_markets_file_path)

# Seed Structures:
require_relative 'seeds/structures.rb'
structures_file_path = File.join(__dir__, 'seeds', 'data', 'structures.csv')
Seeds::Structures.import(structures_file_path) if File.exists?(structures_file_path)

# Seed MarketTargets:
require_relative 'seeds/market_targets.rb'
market_targets_file_path = File.join(__dir__, 'seeds', 'data', 'market_targets.csv')
Seeds::MarketTargets.import(market_targets_file_path) if File.exists?(market_targets_file_path)


require_relative 'seeds/functions.rb'
functions_file_path = File.join(__dir__, 'seeds', 'data', 'functions.csv')
Seeds::Functions.import(functions_file_path) if File.exists?(functions_file_path)

# Seed Regions:
require_relative 'seeds/regions.rb'
Seeds::Regions.new(regions_list: REGIONS_LIST).import

# Adding organizations:
require_relative 'seeds/csv_importers/organizations_csv_importer.rb'

organizations_csv_dir_path = Rails.root.join('db', 'seeds', 'data', 'organizations*.csv')
organizations_csv_files_paths = Dir.glob(organizations_csv_dir_path)

organizations_csv_files_paths.each do |csv_file_path|
  if File.exists?(csv_file_path)
    Seeds::CsvImporters::OrganizationsCsvImporter.import(csv_file_path: csv_file_path)
  else
    puts "[Organization - Upsert] CSV file: '#{csv_file_path}' doesn't exists!!"
  end
end
