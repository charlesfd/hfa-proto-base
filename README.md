# HFA-Proto-Base

_This project is used for testing purpose_

Welcome to this fantastic project where a lot of work has been put into, in a short amount of time, it's the future of the recruiting platform... Or it will be with your help :)

## Setup the project in your environment

- Clone the project in your computer
- Ensure that you have the proper version of ruby defined in the ruby-version and then execute bundler.
- Create the database by running `bundle exec rails db:create`
- Load the database schema `bundle exec rails db:schema:load`
- Run the seeds `bundle exec rails db:seed`

And you're ready to start!!!

## The application

For the moment this application allow the management (creation, update, deletion) of Organizations and Missions.

### Details

- Organizations:
  - To create/update an organization, you need to ensure that the name is present and that all the tags (areas, business models, structures and market targets) have been sets.
  - From the index page you can search for organizations per their names.
  - The index is displaying a maximum of 20 organizations per page.
- Missions:
  - To create/update a mission, your need to ensure that title, organization name, function and region are filled.
  - Index page allow to access to the search related to the mission or to delete it.
  - The search should indicate the mission tags and a potential list of matching organizations (but it seems like it don't work and we don't know why...).

## QA automation work and expectations

A lot of work has been done in a short time but is our application is responding to the quality we should expect?
Your goal, if you accept it, will be to create the necessary tests and report the results (failures and successes) to the development team.

*Don't fork this project*

- To do your work you will be able to use JavaScript or Ruby solutions.
- Your will need to provide a Github/Gitlab link to your cloned repository with the following informations:
  - Number of tests created (in the README).
  - Number of issues (in the README).
  - Technical choices decisions report (in the README).
  - Your feedback about the tests (good part, less interesting part, amount of time passed on it, etc. in the README)
  - In a separate directory create a list of bugs that the team will need to fix (1 report per markdown file)
