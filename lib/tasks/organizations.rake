namespace :organizations do
  desc "Insert organization in database based on a provided JSON file"
  task :insert => :environment do
    require 'json'

    file_path = Rails.root.join("tmp", "organizations.json")
    json_file = File.read(file_path)
    data = JSON.parse(json_file)

    data.each do |datum|
      org_hash = {
        name: datum['name'],
        categories: Array(datum['categories']).map { |d| d['name'] }.compact.map(&:downcase).uniq,
        nb_employees_min: datum['nbEmployeesMin'],
        nb_employees_max: datum['nbEmployeesMax'],
      }

      Organization.create!(org_hash)
    end
  end

  desc "Upsert organizations in database based on Love Affair CSV file"
  task :upsert => :environment do
    require 'csv'
    require Rails.root.join('db', 'seeds', 'csv_importers', 'base')
    require Rails.root.join('db', 'seeds', 'csv_importers', 'organizations_csv_importer')

    organizations_csv_dir_path = Rails.root.join('db', 'seeds', 'data', 'organizations*.csv')
    organizations_csv_files_paths = Dir.glob(organizations_csv_dir_path)

    organizations_csv_files_paths.each do |csv_file_path|
      if File.exists?(csv_file_path)
        Seeds::CsvImporters::OrganizationsCsvImporter.import(csv_file_path: csv_file_path)
      else
        puts "[Organization - Upsert] CSV file: '#{csv_file_path}' doesn't exists!!"
      end
    end

  end
end
